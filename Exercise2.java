package main.java.bigc;

public class Exercise2 {
	public static void main(String[] args) {

		String input = "HELLOOO";

		run_len_encoding(input);

	}

	private static void run_len_encoding(String input) {

		StringBuilder builder = new StringBuilder();
		String[] int_split = input.split("");

		for (int i = 0; i < int_split.length; i++) {
			int cnt = 1;
			for (int j = i + 1; j < int_split.length; j++)
				if (int_split[i].equals(int_split[j])) {
					cnt++;
					i++;
				} else {
					break;
				}
			builder.append(String.valueOf(cnt) + int_split[i]);
		}
		System.out.println(builder);
	}

}

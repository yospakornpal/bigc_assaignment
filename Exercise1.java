package main.java.bigc;

public class Exercise1 {
	public static void main(String[] args) {

		String input1 = "abacabadabacaba";
		mysolution(input1);

		String input2 = "programming";
		mysolution(input2);

	}

	private static void mysolution(String input) {

		String[] int_split = input.split("");

		for (int i = 0; i < int_split.length; i++) {

			int cnt = 0;
			for (int j = 0; j < int_split.length; j++) {

				if (int_split[i].equals(int_split[j])) {
					cnt++;
				}
			}
			if (cnt == 1) {
				System.out.println("-" + int_split[i]);
				break;
			} else if ((i == int_split.length - 1) && (cnt > 1)) {
				System.out.println("_");
			}
		}
	}
}
package main

import ("fmt"
        "net/http"
        "github.com/gin-gonic/gin"
        "encoding/csv"
        "log"
        "os"
        "string")

func main() {
    
    router := gin.Default()

	router.GET("/file/:id", func(c *gin.Context) {
		id := c.Param("id")
		
		var str bytes.Buffer
        str.WriteString(id) 
        str.WriteString(".csv") 
        
        filename := str.String()
		
		file, err := os.Open(filename) 
      
        if err != nil { 
            log.Fatal("Error while reading the file", err) 
        } 
      
        defer file.Close() 
        
        type Respose struct {
            code String
            data Record
        }
        
        type Record struct {
            total int
            records []Content
        }
        
        type Content struct {
            ID int
            Name String
            Age int
            Team String
        }
        
         records, err := csv.NewReader(file).ReadAll()
         
         var contentRecords []Content
        for _, record := range records {
            data := CsvLine{
                ID: strconv.Atoi(record[0]),
                Name: record[1],
                Age: strconv.Atoi(record[2]),
                Team: record[3],
            }
            contentRecords = append(contentRecords, data)
        }
        
        var data Record{len(contentRecords), contentRecords}
        
        var response Respose{"success", data}
        
        c.JSON(http.StatusOK, response)
        return
	})

	router.Run(":8080")
}
